package org.dkni.experimental;

import org.dkni.experimental.service.CountryService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by DK on 08.01.2016.
 */
public class TestEngine {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("META-INF/SpringframeworkConfiguration.xml");
        CountryService svc = (CountryService) ctx.getBean("countryService");
        System.out.println(svc.getCountryById(4L));
    }
}
