package org.dkni.experimental.dao;

import org.dkni.experimental.domain.City;

import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
public interface CityDao {
    void addCity(City city);
    City getCityById(Long id);
    List<City> getAllCities();
}
