package org.dkni.experimental.dao;

import org.dkni.experimental.domain.City;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
@Repository("cityDao")
public class CityDaoImpl implements CityDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void addCity(City city) {
        em.persist(city);
    }

    public City getCityById(Long id) {
        return em.find(City.class, id);
    }

    public List<City> getAllCities() {
        return em.createQuery("from city", City.class).getResultList();
    }
}

