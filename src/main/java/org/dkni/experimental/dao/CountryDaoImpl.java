package org.dkni.experimental.dao;

import org.dkni.experimental.domain.Country;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
@Repository("countryDao")
public class CountryDaoImpl implements CountryDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void addCountry(Country country) {
        em.persist(country);
    }

    public Country getCountryById(Long id) {
        return em.find(Country.class, id);
    }

    @Transactional
    public void updateCountry(Country country){
        em.merge(country);
    }

    @Transactional
    public void removeCountry(Long id){
        em.remove(em.find(Country.class, id));
    }

    public List<Country> getAllCountries() {
        return em.createQuery("from country", Country.class).getResultList();
    }
}
