package org.dkni.experimental.dao;

import org.dkni.experimental.domain.Hotel;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
@Repository("hotelDao")
public class HotelDaoImpl implements HotelDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void addHotel(Hotel hotel) {
        em.persist(hotel);
    }

    public Hotel getHotelById(Long id) {
        return em.find(Hotel.class, id);
    }

    public List<Hotel> getAllHotels() {
        return em.createQuery("from hotel", Hotel.class).getResultList();
    }
}

