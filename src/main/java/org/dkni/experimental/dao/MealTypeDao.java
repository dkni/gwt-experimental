package org.dkni.experimental.dao;

import org.dkni.experimental.domain.MealType;

import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
public interface MealTypeDao {
    void addMealType(MealType mealType);
    MealType getMealTypeById(Long id);
    List<MealType> getAllMealTypes();
}
