package org.dkni.experimental.dao;

import org.dkni.experimental.domain.MealType;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
@Repository("mealTypeDao")
public class MealTypeDaoImpl implements MealTypeDao {

    @PersistenceContext
    private EntityManager em;

    public void addMealType(MealType mealType) {
        em.persist(mealType);
    }

    public MealType getMealTypeById(Long id) {
        return em.find(MealType.class, id);
    }

    public List<MealType> getAllMealTypes() {
        return em.createQuery("from meal_type", MealType.class).getResultList();
    }
}
