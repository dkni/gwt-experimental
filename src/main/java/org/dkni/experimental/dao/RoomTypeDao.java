package org.dkni.experimental.dao;

import org.dkni.experimental.domain.RoomType;

import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
public interface RoomTypeDao {
    void addRoomType(RoomType roomType);
    RoomType getRoomTypeById(Long id);
    List<RoomType> getAllRoomTypes();
}
