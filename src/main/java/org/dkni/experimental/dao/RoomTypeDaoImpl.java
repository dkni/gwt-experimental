package org.dkni.experimental.dao;

import org.dkni.experimental.domain.RoomType;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
@Repository("roomTypeDao")
public class RoomTypeDaoImpl implements RoomTypeDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void addRoomType(RoomType roomType) {
        em.persist(roomType);
    }

    public RoomType getRoomTypeById(Long id) {
        return em.find(RoomType.class, id);
    }

    public List<RoomType> getAllRoomTypes() {
        return em.createQuery("from room_type", RoomType.class).getResultList();
    }
}