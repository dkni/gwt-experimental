package org.dkni.experimental.domain;

import javax.xml.bind.annotation.*;
import javax.persistence.*;
import java.io.Serializable;

@Entity(name="city")
@XmlRootElement(name="city")
@XmlAccessorType(XmlAccessType.FIELD)

public class City implements Serializable {

	private static final long serialVersionUID = -1798067754998324676L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	@XmlAttribute(name = "id")
	private Long id;

	@Column(name="city_code")
	@XmlElement(name="city_code")
	private String code;

	@Column(name="city_name")
	@XmlElement(name = "city_name")
	private String name;

	@Column(name="description")
	@XmlElement(name="description")
	private String description;

	@ManyToOne
	@XmlElement(name="country_id")
	private Country country;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Country getCountry() {
		return country;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
}
