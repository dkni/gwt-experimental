package org.dkni.experimental.domain;

import javax.xml.bind.annotation.*;
import javax.persistence.*;
import java.io.Serializable;

@Entity(name="country")
@XmlRootElement(name="country")
@XmlAccessorType(XmlAccessType.FIELD)

public class Country implements Serializable {

	private static final long serialVersionUID = -1798067754993154676L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	@XmlAttribute(name = "id")
	private Long id;

	@Column(name="country_code")
	@XmlElement(name="country_code")
	private String code;

	@Column(name="country_name")
	@XmlElement(name = "country_name")
	private String name;

	@Column(name="description")
	@XmlElement(name="description")
	private String description;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString(){
		return this.name;
	}
}
