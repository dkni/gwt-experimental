package org.dkni.experimental.domain;

import javax.xml.bind.annotation.*;
import javax.persistence.*;
import java.io.Serializable;

@Entity(name="hotel")
@XmlRootElement(name="hotel")
@XmlAccessorType(XmlAccessType.FIELD)

public class Hotel implements Serializable {
	
	private static final long serialVersionUID = -1798062386993154676L; 
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	@XmlAttribute(name = "id")
	private Long id;
	
	@Column(name="hotel_name")
	@XmlElement(name = "hotel_name")
	private String name;
	
	@Column(name="address")
	@XmlElement(name = "address")
	private String address;
	
	@Column(name="phone")
	@XmlElement(name="phone")
	private String phone;
	
	@Column(name="rating")
	@XmlElement(name="rating")
	private int rating;
	
	@Column(name="description")
	@XmlElement(name="description")
	private String description;
	
	@ManyToOne
	@XmlElement(name="city_id")
	private City city;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getPhone() {
		return phone;
	}

	public int getRating() {
		return rating;
	}

	public String getDescription() {
		return description;
	}

	public City getCity() {
		return city;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCity(City city) {
		this.city = city;
	}
}
