package org.dkni.experimental.domain;

import javax.xml.bind.annotation.*;
import javax.persistence.*;
import java.io.Serializable;

@Entity(name="room_type")
@XmlRootElement(name="room_type")
@XmlAccessorType(XmlAccessType.FIELD)

public class RoomType implements Serializable {

	private static final long serialVersionUID = -1798067881998324676L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	@XmlAttribute(name = "id")
	private Long id;

	@Column(name="room_code")
	@XmlElement(name="room_code")
	private String code;

	@Column(name="room_name")
	@XmlElement(name = "room_name")
	private String name;

	@Column(name="occupancy")
	@XmlElement(name="occupancy")
	private int occupancy;

	@Column(name="description")
	@XmlElement(name="description")
	private String description;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public int getOccupancy() {
		return occupancy;
	}

	public String getDescription() {
		return description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOccupancy(int occupancy) {
		this.occupancy = occupancy;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
