package org.dkni.experimental.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.dkni.experimental.service.CountryService;
import org.dkni.experimental.domain.Country;

import java.util.List;

@Component
@Path("/country")
public class CountryResourceImpl {

    @Autowired
    CountryService countryService;

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getCountryById(@PathParam("id") Long id) {
        if(id == null) {
            return Response.serverError().entity("Country ID cannot be blank").build();
        }
        Country country = countryService.getCountryById(id);
        if (country == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("No country with ID " + id).build();
        }
        return Response.ok(country /*, MediaType.APPLICATION_JSON*/).build();
    }

    /*@POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateCountry(@FormParam("country") Country country){
        countryService.updateCountry(country);
        return Response.ok().entity("Successfully updated country with ID " + country.getId).build();
    }*/

    @DELETE
    @Path("remove/{id}")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response removeCountry(@PathParam("id") Long id){
        if(id == null) {
            return Response.serverError().entity("Country ID cannot be blank").build();
        }
        countryService.removeCountry(id);
        String responseString = "Country with ID " + id + " was successfully removed from database";
        return Response.ok().entity(responseString).build();
    }

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Country> getAllCountries(){
        List<Country> countries = countryService.getAllCountries();
        return countries;
    }
}
