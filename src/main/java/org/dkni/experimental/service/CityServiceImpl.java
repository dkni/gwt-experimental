package org.dkni.experimental.service;

import org.dkni.experimental.dao.CityDao;
import org.dkni.experimental.domain.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
@Service("cityService")
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDao dao;

    @Transactional
    public void addCity(City city) {
        dao.addCity(city);
    }

    public City getCityById(Long id) {
        return dao.getCityById(id);
    }

    public List<City> getAllCities() {
        return dao.getAllCities();
    }
}
