package org.dkni.experimental.service;

import org.dkni.experimental.domain.Country;

import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
public interface CountryService {
    void addCountry(Country country);
    Country getCountryById(Long id);
    void updateCountry(Country country);
    void removeCountry(Long id);
    List<Country> getAllCountries();
}
