package org.dkni.experimental.service;

import org.dkni.experimental.dao.CountryDao;
import org.dkni.experimental.domain.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
@Service("countryService")
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryDao dao;

    @Transactional
    public void addCountry(Country country) {
        dao.addCountry(country);
    }

    public Country getCountryById(Long id) {
        return dao.getCountryById(id);
    }

    public void updateCountry(Country country){
        dao.updateCountry(country);
    }
    
    public void removeCountry(Long id){
       dao.removeCountry(id);
    }

    public List<Country> getAllCountries() {
        return dao.getAllCountries();
    }
}
