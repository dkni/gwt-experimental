package org.dkni.experimental.service;

import org.dkni.experimental.domain.Hotel;

import java.util.List;

/**
 * Created by DK on 08.01.2016.
 */
public interface HotelService {
    void addHotel(Hotel hotel);
    Hotel getHotelById(Long id);
    List<Hotel> getAllHotels();
}
